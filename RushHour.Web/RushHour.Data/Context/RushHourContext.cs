﻿using Microsoft.EntityFrameworkCore;
using RushHour.Data.Entity;
using System;

namespace RushHour.Data.Context
{
    public class RushHourContext : DbContext
    {
        public RushHourContext(DbContextOptions options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>()
                .HasMany(u => u.Appointments)
                .WithOne();

            modelBuilder.Entity<AppointmentActivity>()
                .HasKey(a => new { a.AppointmentId, a.ActivityId });

            modelBuilder.Entity<AppointmentActivity>()
                .HasOne(aa => aa.Appointment)
                .WithMany(a => a.AppointmentActivities)
                .HasForeignKey(aa => aa.AppointmentId);

            modelBuilder.Entity<AppointmentActivity>()
                .HasOne(aa => aa.Activity)
                .WithMany(a => a.AppointmentActivities)
                .HasForeignKey(aa => aa.ActivityId);

            modelBuilder.Entity<Appointment>()
                .HasOne(a => a.User)
                .WithMany(u => u.Appointments)
                .HasForeignKey(a => a.UserId);

            modelBuilder.Entity<Appointment>()
                .Property(a => a.StartDateTime)
                .IsRequired();

            modelBuilder.Entity<Appointment>()
                .Property(a => a.EndDateTime)
                .IsRequired();

            modelBuilder.Entity<Appointment>()
                .Property(a => a.UserId)
                .IsRequired();

            modelBuilder.Entity<Activity>()
                .Property(a => a.Name)
                .IsRequired();

            modelBuilder.Entity<Activity>()
                .Property(a => a.Duration)
                .IsRequired();

            modelBuilder.Entity<Activity>()
                .Property(a => a.Price)
                .IsRequired();

            modelBuilder.Entity<User>()
                .HasData(new User
                {
                    Id = 1,
                    Email = "admin@gmail.com",
                    Password = "123456",
                    UserName = "admin",
                    Phone = "+359123456"
                });

            modelBuilder.Entity<Appointment>()
                .HasData(new Appointment
                {
                    Id = 1,
                    StartDateTime = new DateTime(2018, 1, 1),
                    EndDateTime = new DateTime(2018, 1, 2),
                    IsCanceled = false,
                    UserId = 1
                });

            modelBuilder.Entity<Activity>()
                .HasData(new Activity
                {
                    Id = 1,
                    Duration = 1,
                    Price = 1,
                    Name = "activity"
                });

            modelBuilder.Entity<AppointmentActivity>()
                .HasData(new AppointmentActivity
                {
                    ActivityId = 1,
                    AppointmentId = 1
                });

            base.OnModelCreating(modelBuilder);
    }

    public DbSet<Appointment> Appointments { get; set; }

    public DbSet<Activity> Activities { get; set; }

    public DbSet<User> Users { get; set; }

    public DbSet<AppointmentActivity> AppointmentActivities { get; set; }
}
}