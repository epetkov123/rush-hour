﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace RushHour.Data.Entity
{
    public class Activity : IBaseEntity
    {
        [Required(AllowEmptyStrings = false)]
        public string Name { get; set; }

        [Required]
        public float Duration { get; set; }

        [Required]
        public decimal Price { get; set; }

        public ICollection<AppointmentActivity> AppointmentActivities { get; set; }
    }
}