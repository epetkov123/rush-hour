﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace RushHour.Data.Entity
{
    public class Appointment : IBaseEntity
    {
        [Required]
        public DateTime StartDateTime { get; set; }

        [Required]
        public DateTime EndDateTime { get; set; }

        [Required]
        public int UserId { get; set; }

        public User User { get; set; }

        public bool IsCanceled { get; set; }

        public ICollection<AppointmentActivity> AppointmentActivities { get; set; }
    }
}