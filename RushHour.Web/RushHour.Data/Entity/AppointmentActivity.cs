﻿namespace RushHour.Data.Entity
{
    public class AppointmentActivity
    {
        public int AppointmentId { get; set; }

        public Appointment Appointment { get; set; }

        public int ActivityId { get; set; }

        public Activity Activity { get; set; }
    }
}
