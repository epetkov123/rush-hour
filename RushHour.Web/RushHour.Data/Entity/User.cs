﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace RushHour.Data.Entity
{
    public class User : IBaseEntity
    {
        [Required(AllowEmptyStrings = false)]
        public string Email { get; set; }

        [Required(AllowEmptyStrings = false)]
        public string Password { get; set; }

        [Required(AllowEmptyStrings = false)]
        public string UserName { get; set; }

        public string Phone { get; set; }

        public ICollection<Appointment> Appointments { get; set; }
    }
}