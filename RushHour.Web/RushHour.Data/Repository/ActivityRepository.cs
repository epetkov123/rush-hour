﻿using RushHour.Data.Context;
using RushHour.Data.Entity;

namespace RushHour.Data.Repository
{
    public class ActivityRepository : BaseRepository<Activity>, IActivityRepository
    {
        public ActivityRepository(RushHourContext context)
            : base(context)
        {
        }
    }
}