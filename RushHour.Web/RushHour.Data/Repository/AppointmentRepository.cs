﻿using Microsoft.EntityFrameworkCore;
using RushHour.Data.Context;
using RushHour.Data.Entity;
using System.Linq;

namespace RushHour.Data.Repository
{
    public class AppointmentRepository : BaseRepository<Appointment>, IAppointmentRepository
    {
        protected new DbSet<Appointment> dbSet;

        public AppointmentRepository(RushHourContext context)
            : base(context)
        {
            dbSet = context.Set<Appointment>();
        }

        public new IQueryable GetMine(int id)
        {
            return dbSet.Where(i => i.UserId == id)
                .Include(appointment => appointment.AppointmentActivities)
                .ThenInclude(a => a.Activity);
        }
    }
}