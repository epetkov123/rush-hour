﻿using Microsoft.EntityFrameworkCore;
using RushHour.Data.Context;
using RushHour.Data.Entity;
using System.Linq;

namespace RushHour.Data.Repository
{
    public class BaseRepository<T> : IBaseRepository<T> where T : IBaseEntity
    {
        protected RushHourContext context;
        protected DbSet<T> dbSet;

        public BaseRepository(RushHourContext context)
        {
            this.context = context;
            dbSet = context.Set<T>();
        }

        public void Create(T item)
        {
            dbSet.Add(item);
        }

        public IQueryable Get()
        {
            return dbSet;
        }

        public T Get(int id)
        {
            return dbSet.Find(id);
        }

        public void Update(T item)
        {
            dbSet.Update(item);
        }

        public void Delete(T item)
        {
            dbSet.Remove(item);
        }

        public void Save()
        {
            context.SaveChanges();
        }

        public IQueryable GetMine(int id)
        {
            throw new System.NotImplementedException();
        }
    }
}