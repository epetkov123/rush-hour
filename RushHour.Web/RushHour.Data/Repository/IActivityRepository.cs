﻿using RushHour.Data.Entity;

namespace RushHour.Data.Repository
{
    public interface IActivityRepository : IBaseRepository<Activity>
    {
    }
}
