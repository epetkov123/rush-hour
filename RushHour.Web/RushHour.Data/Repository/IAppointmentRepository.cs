﻿using RushHour.Data.Entity;
using System.Linq;

namespace RushHour.Data.Repository
{
    public interface IAppointmentRepository : IBaseRepository<Appointment>
    {
    }
}
