﻿using System.Linq;

namespace RushHour.Data.Repository
{
    public interface IBaseRepository<T>
    {
        void Create(T item);

        IQueryable Get();

        T Get(int id);

        void Update(T item);

        void Delete(T item);

        void Save();

        IQueryable GetMine(int id);
    }
}