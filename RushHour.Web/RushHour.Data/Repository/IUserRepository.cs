﻿using RushHour.Data.Entity;

namespace RushHour.Data.Repository
{
    public interface IUserRepository : IBaseRepository<User>
    {
        User GetByEmailAndPassword(string email, string password);

        bool CheckIfUserExists(string email, string username);

        void RegisterUser(User user);
    }
}
