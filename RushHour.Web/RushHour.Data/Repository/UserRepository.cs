﻿using RushHour.Data.Context;
using RushHour.Data.Entity;
using System.Linq;

namespace RushHour.Data.Repository
{
    public class UserRepository : BaseRepository<User>, IUserRepository
    {
        public UserRepository(RushHourContext context)
           : base(context)
        {
        }

        public User GetByEmailAndPassword(string email, string password)
        {
            return context.Users.FirstOrDefault(u => u.Email == email && u.Password == password);
        }

        public bool CheckIfUserExists(string email, string username)
        {
            if ((context.Users.Where(u => u.UserName == username || u.Email == email).Any()))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public void RegisterUser(User user)
        {
            context.Add(user);
        }
    }
}