﻿using RushHour.Data.Entity;
using RushHour.Service.Service;

namespace RushHour.Service
{
    public class AuthenticationService
    {
        private IUserService service;

        public AuthenticationService(IUserService service)
        {
            this.service = service;
        }

        public User LoggedUser { get; private set; }

        public void AuthenticateUser(string email, string password)
        {
            LoggedUser = service.GetByEmailAndPassword(email, password);
        }
    }
}