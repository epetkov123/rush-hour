﻿using RushHour.Data.Entity;
using RushHour.Data.Repository;

namespace RushHour.Service.Service
{
    public class ActivityService : BaseService<Activity>, IActivityService
    {
        public ActivityService(IActivityRepository repository)
            : base(repository)
        {
        }
    }
}