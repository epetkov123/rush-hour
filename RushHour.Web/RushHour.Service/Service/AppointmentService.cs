﻿using RushHour.Data.Entity;
using RushHour.Data.Repository;
using System.Linq;

namespace RushHour.Service.Service
{
    public class AppointmentService : BaseService<Appointment>, IAppointmentService
    {
        public AppointmentService(IAppointmentRepository repository)
            : base(repository)
        {
        }

        public IQueryable GetMine(int id)
        {
            return repository.GetMine(id);
        }
    }
}