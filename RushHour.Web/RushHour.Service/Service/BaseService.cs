﻿using RushHour.Data.Entity;
using RushHour.Data.Repository;
using System.Linq;

namespace RushHour.Service.Service
{
    public class BaseService<T> : IBaseService<T> where T : IBaseEntity
    {
        protected IBaseRepository<T> repository;

        public BaseService(IBaseRepository<T> repository)
        {
            this.repository = repository;
        }

        public void Create(T item)
        {
            repository.Create(item);
        }

        public IQueryable Get()
        {
            return repository.Get();
        }

        public T Get(int id)
        {
            return repository.Get(id);
        }

        public void Update(T item)
        {
            repository.Update(item);
        }

        public void Delete(int id)
        {
            repository.Delete(repository.Get(id));
        }

        public void Save()
        {
            repository.Save();
        }
    }
}