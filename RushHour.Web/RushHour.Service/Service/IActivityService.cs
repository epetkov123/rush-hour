﻿using RushHour.Data.Entity;

namespace RushHour.Service.Service
{
    public interface IActivityService : IBaseService<Activity>
    {
    }
}
