﻿using RushHour.Data.Entity;
using System.Linq;

namespace RushHour.Service.Service
{
    public interface IAppointmentService : IBaseService<Appointment>
    {
        IQueryable GetMine(int id);
    }
}