﻿using System.Linq;

namespace RushHour.Service.Service
{
    public interface IBaseService<T> 
    {
        void Create(T item);

        IQueryable Get();

        T Get(int id);

        void Update(T item);

        void Delete(int id);

        void Save();
    }
}