﻿using RushHour.Data.Entity;

namespace RushHour.Service.Service
{
    public interface IUserService : IBaseService<User> 
    {
        User GetByEmailAndPassword(string email, string password);

        void RegisterUser(User user);

        bool CheckIfUserExists(string email, string username);
    }
}