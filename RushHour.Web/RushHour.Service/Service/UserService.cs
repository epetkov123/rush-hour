﻿using RushHour.Data.Entity;
using RushHour.Data.Repository;

namespace RushHour.Service.Service
{
    public class UserService : BaseService<User>, IUserService
    {
        private new IUserRepository repository;

        public UserService(IUserRepository repository)
            :base(repository)
        {
            this.repository = repository;
        }

        public User GetByEmailAndPassword(string email, string password)
        {
            return repository.GetByEmailAndPassword(email, password);
        }

        public void RegisterUser(User user)
        {
            repository.RegisterUser(user);
        }

        public bool CheckIfUserExists(string email, string username)
        {
            return repository.CheckIfUserExists(email, username);
        }
    }
}