﻿using AutoMapper;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using RushHour.Data.Entity;
using RushHour.Service.Helpers;
using RushHour.Service.Service;
using RushHour.Web.Models;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;

namespace RushHour.Web.Controllers
{
    public class AccountController : Controller
    {
        private IUserService service;

        private readonly IMapper mapper;

        public AccountController(IUserService service, IMapper mapper)
        {
            this.service = service;
            this.mapper = mapper;
        }

        [HttpGet]
        public IActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Login(LoginViewModel vm)
        {
            if (ModelState.IsValid)
            {
                AccountManager accountManager = new AccountManager(new Service.AuthenticationService(service));
                accountManager.Authenticate(vm.Email, vm.Password);

                if (accountManager.LoggedUser() != null)
                {
                    string role = "";
                    if (accountManager.LoggedUser().Id == 1)
                    {
                        role = "Administrator";
                    }
                    else
                    {
                        role = "User";
                    }

                    List<Claim> claims = new List<Claim> {
                        new Claim(ClaimTypes.Name, accountManager.LoggedUser().Id.ToString()),
                        new Claim(ClaimTypes.Email, vm.Email),
                        new Claim(ClaimTypes.Role, role)
                    };

                    ClaimsIdentity identity = new ClaimsIdentity(claims, "cookie");

                    ClaimsPrincipal principal = new ClaimsPrincipal(identity);

                    await HttpContext.SignInAsync(
                            scheme: CookieAuthenticationDefaults.AuthenticationScheme,
                            principal: principal,
                            properties: new AuthenticationProperties
                            {
                                IsPersistent = vm.RememberMe,
                                ExpiresUtc = DateTime.UtcNow.AddMinutes(30),
                                AllowRefresh = true
                            });

                    HttpContext.Session.SetString("name", vm.Email);
                    HttpContext.Session.SetString("id", accountManager.LoggedUser().Id.ToString());

                    if (accountManager.LoggedUser().Id == 1)
                    {
                        return RedirectToAction("AdministratorView", "Home");
                    }

                    return RedirectToAction("UserView", "Home");
                }

                ModelState.AddModelError("", "Invalid login");
            }
            return View(vm);
        }

        [HttpGet]
        public IActionResult Register()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Register(RegisterViewModel vm)
        {
            if (ModelState.IsValid)
            {
                if (service.CheckIfUserExists(vm.Email, vm.UserName) == false)
                {
                    var item = mapper.Map(vm, new User());

                    service.RegisterUser(item);
                    service.Save();

                    EmailSender.sendToEmail(vm.UserName, vm.Email);
                    return RedirectToAction("Index", "Home");
                }
                ModelState.AddModelError("", "Username or email already exists");
            }
            return View(vm);
        }

        [HttpGet]
        public async Task<IActionResult> Logout()
        {
            await HttpContext.SignOutAsync(
                scheme: CookieAuthenticationDefaults.AuthenticationScheme);

            var sessionKey = HttpContext.Session;

            HttpContext.Session.Clear();

            return RedirectToAction("Index", "Home");
        }
    }
}