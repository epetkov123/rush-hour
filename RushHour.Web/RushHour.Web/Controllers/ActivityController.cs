﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.AspNetCore.Mvc;
using RushHour.Data.Entity;
using RushHour.Service.Service;
using RushHour.Web.Models;
using System.Collections.Generic;
using System.Linq;

namespace RushHour.Web.Controllers
{
    public class ActivityController : Controller
    {
        private IActivityService service;

        private readonly IMapper mapper;

        public ActivityController(IActivityService service, IMapper mapper)
        {
            this.service = service;
            this.mapper = mapper;
        }

        [HttpGet]
        public IActionResult GetAll()
        {
            IList<ActivityViewModel> model = service.Get()
                .ProjectTo<ActivityViewModel>().ToList();

            return View("ViewAll", model);
        }

        [HttpGet]
        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Create(ActivityViewModel vm)
        {
            var item = Mapper.Map(vm, new Activity());

            service.Create(item);
            service.Save();

            return RedirectToAction("GetAll");
        }

        [HttpGet]
        public IActionResult Edit(int id)
        {
            var item = Mapper.Map(service.Get(id), new ActivityViewModel());

            return View(item);
        }

        [HttpPost]
        public IActionResult Edit(ActivityViewModel vm)
        {
            var item = Mapper.Map(vm, new Activity());

            service.Update(item);
            service.Save();

            return RedirectToAction("GetAll");
        }

        [HttpGet]
        public IActionResult Delete(int id)
        {
            service.Delete(id);
            service.Save();

            return RedirectToAction("GetAll");
        }
    }
}