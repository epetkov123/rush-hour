﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using RushHour.Data.Entity;
using RushHour.Service.Service;
using RushHour.Web.Models;
using System.Collections.Generic;
using System.Linq;

namespace RushHour.Web.Controllers
{
    public class AppointmentController : Controller
    {
        private IAppointmentService appointmentService;

        private IActivityService activityService;

        private readonly IMapper mapper;

        public AppointmentController(IAppointmentService appointmentService, IActivityService activityService, IMapper mapper)
        {
            this.appointmentService = appointmentService;
            this.activityService = activityService;
            this.mapper = mapper;
        }

        [HttpGet]
        public IActionResult GetAll()
        {
            IQueryable activities = activityService.Get();

            IList<AppointmentViewModel> model = appointmentService.Get()
                .ProjectTo<AppointmentViewModel>().ToList();

            return View("ViewAll", model);
        }

        [HttpGet]
        public IActionResult GetMine()
        {
            int userId = int.Parse(HttpContext.Session.GetString("id"));

            IList<AppointmentViewModel> model = mapper.Map<IQueryable, IList<AppointmentViewModel>>
                (appointmentService.GetMine(userId));
            /*
            IList<AppointmentViewModel> model = appointmentService.GetMine(userId)
                .ProjectTo<AppointmentViewModel>().ToList();*/

            return View("ViewMine", model);
        }

        [HttpGet]
        public IActionResult GetMineUser()
        {
            int userId = int.Parse(HttpContext.Session.GetString("id"));

            IList<AppointmentViewModel> model = mapper.Map<IQueryable, IList<AppointmentViewModel>>
                (appointmentService.GetMine(userId));
            /*
            IList<AppointmentViewModel> model = appointmentService.GetMine(userId)
                .ProjectTo<AppointmentViewModel>().ToList();*/

            return View("ViewMineUser", model);
        }

        [HttpGet]
        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Create(AppointmentViewModel model)
        {
            var item = Mapper.Map(model, new Appointment());
            item.UserId = int.Parse(HttpContext.Session.GetString("id"));

            appointmentService.Create(item);
            appointmentService.Save();

            return RedirectToAction("GetMine");
        }

        [HttpGet]
        public IActionResult CreateByUser()
        {
            return View();
        }

        [HttpPost]
        public IActionResult CreateByUser(AppointmentViewModel model)
        {
            var item = Mapper.Map(model, new Appointment());
            item.UserId = int.Parse(HttpContext.Session.GetString("id"));

            appointmentService.Create(item);
            appointmentService.Save();

            return RedirectToAction("GetMineUser");
        }

        [HttpGet]
        public IActionResult Edit(int id)
        {
            var item = Mapper.Map(appointmentService.Get(id), new AppointmentViewModel());

            return View(item);
        }

        [HttpGet]
        public IActionResult EditMine(int id)
        {
            var item = Mapper.Map(appointmentService.Get(id), new AppointmentViewModel());

            return View(item);
        }

        [HttpPost]
        public IActionResult EditMine(AppointmentViewModel model)
        {
            var item = Mapper.Map(model, new Appointment());

            appointmentService.Update(item);
            appointmentService.Save();

            return RedirectToAction("GetMine");
        }

        [HttpGet]
        public IActionResult EditMineUser(int id)
        {
            var item = Mapper.Map(appointmentService.Get(id), new AppointmentViewModel());

            return View(item);
        }

        [HttpPost]
        public IActionResult EditMineUser(AppointmentViewModel model)
        {
            var item = Mapper.Map(model, new Appointment());

            appointmentService.Update(item);
            appointmentService.Save();

            return RedirectToAction("GetMineUser");
        }

        [HttpPost]
        public IActionResult Edit(AppointmentViewModel model)
        {
            var item = Mapper.Map(model, new Appointment());

            appointmentService.Update(item);
            appointmentService.Save();

            return RedirectToAction("GetAll");
        }

        [HttpGet]
        public IActionResult Delete(int id)
        {
            appointmentService.Delete(id);
            appointmentService.Save();

            return RedirectToAction("GetMine");
        }

        [HttpGet]
        public IActionResult DeleteByUser(int id)
        {
            appointmentService.Delete(id);
            appointmentService.Save();

            return RedirectToAction("GetMineUser");
        }
    }
}