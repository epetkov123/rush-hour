﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using RushHour.Web.Models;

namespace RushHour.Web.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            if (HttpContext.Session.IsAvailable)
            {
                if (HttpContext.User.IsInRole("Administrator"))
                {
                    return RedirectToAction("AdministratorView");
                }
                else
                {
                    return RedirectToAction("UserView");
                }

            }
            else
            {
                return View();
            }
        }

        [Authorize(Roles = "Administrator")]
        public IActionResult AdministratorView()
        {
            return View();
        }

        [Authorize(Roles = "User")]
        public IActionResult UserView()
        {
            return View();
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = System.Diagnostics.Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}