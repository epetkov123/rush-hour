﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.AspNetCore.Mvc;
using RushHour.Data.Entity;
using RushHour.Service.Service;
using RushHour.Web.Models;
using System.Collections.Generic;
using System.Linq;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace RushHour.Web.Controllers.RestControllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    public class ActivityController : Controller
    {
        private IActivityService activityService;

        private IMapper mapper;

        public ActivityController(IActivityService activityService, IMapper mapper)
        {
            this.activityService = activityService;
            this.mapper = mapper;
        }

        [HttpPost]
        public IActionResult Create([FromBody]ActivityViewModel activityModel)
        {
            var item = mapper.Map(activityModel, new Activity());

            activityService.Create(item);
            activityService.Save();

            return CreatedAtRoute("GetActivity", new { id = item.Id }, item);
        }

        [HttpGet]
        public List<ActivityViewModel> GetAll()
        {
            return activityService.Get().ProjectTo<ActivityViewModel>().ToList();
        }

        [HttpGet("{id}", Name = "GetActivity")]
        public ActivityViewModel GetById(int id)
        {
            return mapper.Map(activityService.Get(id), new ActivityViewModel());
        }

        [HttpPut("{id}")]
        public IActionResult Update(int id, [FromBody] ActivityViewModel activityViewModel)
        {
            var item = activityService.Get(id);

            if (item == null)
            {
                return NotFound();
            }

            item.Name = activityViewModel.Name;
            item.Price = activityViewModel.Price;
            item.Duration = activityViewModel.Duration;
            item.AppointmentActivities = activityViewModel.AppointmentActivities;

            activityService.Update(item);
            activityService.Save();

            return Ok();
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            if (activityService.Get(id) == null)
            {
                return NotFound();
            }

            activityService.Delete(id);
            activityService.Save();

            return Ok();
        }
    }
}