﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using RushHour.Data.Entity;
using RushHour.Service.Service;
using RushHour.Web.Models;
using System.Collections.Generic;
using System.Linq;

namespace RushHour.Web.Controllers
{
    public class UserController : Controller
    {
        private IUserService service;

        private readonly IMapper mapper;

        public UserController(IUserService service, IMapper mapper)
        {
            this.service = service;
            this.mapper = mapper;
        }

        [HttpGet]
        public IActionResult GetMyProfile()
        {
            int userId = int.Parse(HttpContext.Session.GetString("id"));
            
            var item = mapper.Map(service.Get(userId), new UserViewModel());

            return View("MyProfile", item);
        }

        [HttpGet]
        public IActionResult GetMyProfileUser()
        {
            int userId = int.Parse(HttpContext.Session.GetString("id"));

            var item = mapper.Map(service.Get(userId), new UserViewModel());

            return View("MyProfileUser", item);
        }

        [HttpGet]
        public IActionResult GetAll()
        {
            IList<UserViewModel> model = service.Get()
                .ProjectTo<UserViewModel>().ToList();

            return View("View", model);
        }

        [HttpGet]
        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Create(UserViewModel vm)
        {
            var item = Mapper.Map(vm, new User());

            service.Create(item);
            service.Save();

            return RedirectToAction("GetAll");
        }

        [HttpGet]
        public IActionResult Delete(int id)
        {
            service.Delete(id);
            service.Save();

            return RedirectToAction("GetAll");
        }

        [HttpGet]
        public IActionResult Edit(int id)
        {
            var item = mapper.Map(service.Get(id), new UserViewModel());

            return View(item);
        }

        [HttpPost]
        public IActionResult Edit(UserViewModel vm)
        {
            var item = mapper.Map(vm, new User());

            service.Update(item);
            service.Save();

            return RedirectToAction("GetAll");
        }

        [HttpGet]
        public IActionResult EditProfile(int id)
        {
            var item = Mapper.Map(service.Get(id), new UserViewModel());

            return View(item);
        }

        [HttpPost]
        public IActionResult EditProfile(UserViewModel vm)
        {
            var item = mapper.Map(vm, new User());

            service.Update(item);
            service.Save();

            return RedirectToAction("GetMyProfile");
        }

        [HttpGet]
        public IActionResult EditProfileUser(int id)
        {
            var item = Mapper.Map(service.Get(id), new UserViewModel());

            return View(item);
        }

        [HttpPost]
        public IActionResult EditProfileUser(UserViewModel vm)
        {
            var item = mapper.Map(vm, new User());

            service.Update(item);
            service.Save();

            return RedirectToAction("GetMyProfileUser");
        }
    }
}