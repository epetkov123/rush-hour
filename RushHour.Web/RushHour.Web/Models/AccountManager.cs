﻿using RushHour.Data.Entity;
using RushHour.Service;

namespace RushHour.Web.Models
{
    public class AccountManager
    {
        private AuthenticationService authenticationService;

        public AccountManager(AuthenticationService authenticationService)
        {
            this.authenticationService = authenticationService;
        }

        public User LoggedUser()
        {
            return authenticationService.LoggedUser;
        }

        public void Authenticate(string email, string password)
        {
            authenticationService.AuthenticateUser(email, password);
        }
    }
}