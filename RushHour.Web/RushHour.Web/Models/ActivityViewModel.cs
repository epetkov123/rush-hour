﻿using RushHour.Data.Entity;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace RushHour.Web.Models
{
    public class ActivityViewModel
    {
        public int Id { get; set; }

        [Required]
        [DataType(DataType.Text)]
        public string Name { get; set; }

        [Required]
        [DataType(DataType.Duration)]
        public float Duration { get; set; }

        [Required]
        [DataType(DataType.Currency)]
        public decimal Price { get; set; }

        public ICollection<AppointmentActivity> AppointmentActivities { get; set; }
    }
}