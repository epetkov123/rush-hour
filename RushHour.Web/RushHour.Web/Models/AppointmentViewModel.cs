﻿using RushHour.Data.Entity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace RushHour.Web.Models
{
    public class AppointmentViewModel
    {
        public int Id { get; set; }

        [Required]
        [DataType(DataType.DateTime)]
        public DateTime StartDateTime { get; set; }

        [Required]
        [DataType(DataType.DateTime)]
        public DateTime EndDateTime { get; set; }

        [Required]
        public int UserId { get; set; }

        public User User { get; set; }

        public bool IsCanceled { get; set; }

        public ICollection<AppointmentActivity> AppointmentActivities { get; set; }
    }
}