﻿using RushHour.Data.Entity;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace RushHour.Web.Models
{
    public class RegisterViewModel
    {
        public int Id { get; set; }

        [Required]
        [EmailAddress]
        [MaxLength(256)]
        [Display(Name = "Email Address")]
        public string Email { get; set; }

        [Required]
        [MinLength(6)]
        [MaxLength(50)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [Required]
        [MinLength(6)]
        [MaxLength(50)]
        [DataType(DataType.Password)]
        [Display(Name = "Confirm Password")]
        [Compare("Password", ErrorMessage = "The password does not match the confirmation password.")]
        public string ConfirmPassword { get; set; }

        [Required]
        public string UserName { get; set; }

        public string Phone { get; set; }

        public ICollection<Appointment> Appointments { get; set; }
    }
}